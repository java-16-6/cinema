<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="container" style="background-image: url(${pageContext.request.contextPath}/resources/images/popcorn-header.jpg); height: 75px;"/>

    <c:choose>
        <c:when test="${pageTitle != null}">
            <h1><spring:message code="${pageTitle}" /></h1>
        </c:when>

        <c:otherwise>
            <h1><spring:message code="default.title" /></h1>
        </c:otherwise>
    </c:choose>


</div>