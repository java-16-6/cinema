<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>List movies with time</title>
</head>
<body>

<p>Select the time of the session</p>
<br/>

<div id="time">
    <c:forEach var="sessionDto" items="${allMovieSessions}">
        <div>
            <h2>Film: ${sessionDto.film.filmId}</h2>
            <c:forEach var="session" items="${sessionDto.sessionList}">
                <a href="/movie/session/${session.movieSessionId}">
                    <fmt:formatDate pattern="HH:mm" value="${session.sessionStartTime}"/>
                </a>
            </c:forEach>
        </div>
    </c:forEach>
</div>



</body>
</html>
