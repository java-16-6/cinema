<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<c:url value='${pageContext.request.contextPath}/resources/css/view-session.css'/>">
    <title>View Session</title>

</head>
<body>


<div id="background">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Home</a>
            </div>
            <button class="btn btn-danger navbar-btn" onclick="getBack()">Назад</button>
            <button class="btn btn-danger navbar-btn" onclick="goToBasket()">Корзина</button>
        </div>
    </nav>
    <%--preloader--%>
    <div id="preloader">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
    <%--preloader--%>
    <div align="center" id="main_container" class="container">
        <div class="box">
            <div align="center" id="seats_list" class="content">

                <img width="100%"
                     src="<c:url value='${pageContext.request.contextPath}/resources/images/screen.png'/>"/>
            </div>
            <div align="left" id="right_menu" class="side_bar">
                <h3>Зал №${hall.hallNumber} <br>
                    Фильм ${movieSession.getFilm().getName()}<br>
                    ${movieSession.getSessionStartDate()} ${movieSession.getSessionStartTime()}<br>
                    <div align="left">
                        <h5><span class="empty_seat"></span>STANDART - ${standartPrice} грн <span
                                class="empty_left_lux_seat"></span><span class="empty_right_lux_seat"></span>LUX
                            - ${comfortPrice} грн</h5>
                    </div>
                    <br>
                    <span class="label label-default">Билеты:</span></h3>
                <br>

            </div>
            <div align="left" id="result" class="result_list">

                <h3 id="summaryPrice"></h3>
                <div style="margin:3em;">
                    <button type="button" class="btn btn-success btn-lg" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order">Добавить в корзину
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    //   preloader //
    var content = document.getElementById("main_container");
    function delPreloaderAndShowContent() {
        var preloader = document.getElementById("preloader");
        content.style.display = "block";


        preloader.remove();
    }
    setTimeout(delPreloaderAndShowContent, 1500);
    //   preloader //


    $('.btn').on('click', function() {
        var $this = $(this);
        $this.button('loading');
        setTimeout(function() {
            $this.button('reset');
        }, 8000);

        var query = [];
        for (let ticket of order.values()) {
            query.push(ticket);
        }
        console.log(query);
        console.log(JSON.stringify(query));
        fetch('${pageContext.request.contextPath}/basket/test', {
            method: 'POST',
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(query)
        })
            .then(setTimeout(goToBasket, 3000))
            .catch(alert);
    });
    <%--function createOrder() {--%>

        <%--var query = [];--%>
        <%--for (let ticket of order.values()) {--%>
            <%--query.push(ticket);--%>
        <%--}--%>
        <%--console.log(query);--%>
        <%--console.log(JSON.stringify(query));--%>
        <%--fetch('${pageContext.request.contextPath}/basket/test', {--%>
            <%--method: 'POST',--%>
            <%--headers: {--%>
                <%--"Accept": "application/json",--%>
                <%--"Content-Type": "application/json"--%>
            <%--},--%>
            <%--body: JSON.stringify(query)--%>
        <%--})--%>
            <%--.then(setTimeout(goToBasket, 2000))--%>
            <%--.catch(alert);--%>
    <%--}--%>

    function goToBasket() {
        window.location = '${pageContext.request.contextPath}/basket/';
    }


    function getBack() {
        history.back();
    }

    function goToBasket() {
        location.href = "${pageContext.request.contextPath}/basket/";
    }

    var order = new Map();
    var summaryPrice = 0;
    var ticketInBucketCounter = 0;
    var table = document.getElementById("seats_list");
    var sideMenu = document.getElementById("right_menu");
    var rowsNumberInHall = ${hall.getRows().size()};
    var standartPrice = ${standartPrice};
    var comfortPrice = ${comfortPrice};
    var movieSessionId = ${movieSession.getMovieSessionId()};
    var hallId = ${hall.getHallId()};


    var row = 1;
    var seat = 1;

    ///
    <c:forEach var="row" items="${hall.getRows()}">
    table.innerHTML = table.innerHTML + '<div align="center" id="table-seats">';
    <c:forEach var="seat" items="${row.seats}">
    var typeOfSeat = ${seat.seatType.getType()};
    if (typeOfSeat === 1) {
        table.innerHTML = table.innerHTML + '<div reserve="empty_seat" price="' + standartPrice + '" id="btn_buy" onclick="" class="empty_seat"><em>' +
            row + " Ряд, " + seat + " место" +
            '<i></i></em></div>';
    } else if (typeOfSeat === 2) {
        if (seat % 2 === 0) {
            table.innerHTML = table.innerHTML + '<div reserve="empty_right_lux_seat" price="' + comfortPrice + '" id="btn_buy" onclick="reserveSeat()" class="empty_right_lux_seat"><em>' +
                row + " Ряд, " + seat + " место" +
                '<i></i></em></div>';
        } else {
            table.innerHTML = table.innerHTML + '<div reserve="empty_left_lux_seat" price="' + comfortPrice + '" id="btn_buy" onclick="reserveSeat()" class="empty_left_lux_seat"><em>' +
                row + " Ряд, " + seat + " место" +
                '<i></i></em></div>';
        }
        updateSumaryPrice()
    }
    ///////////////

    var btn_buy = document.getElementById("btn_buy");

    btn_buy.id = "but" + row + seat;
    btn_buy.setAttribute("row", row);
    btn_buy.setAttribute("seat", seat);
    if (localStorage.getItem(movieSessionId + btn_buy.id) != null) {
        reserveSeat(btn_buy);
    } else {
        btn_buy.onclick = function (event) {
            reserveSeat();
        };
        btn_buy.setAttribute("onclick", "reserveSeat()");
    }

    seat++;

    </c:forEach>
    table.innerHTML = table.innerHTML + '</div>';
    seat = 1;
    row++;
    </c:forEach>




    //добавть резерв места по клику
    function reserveSeat(btnId) {
        if (ticketInBucketCounter < 9) {

            if (btnId != null) {
                var btn = btnId;
                console.log("test111");
            } else {
                console.log(event.currentTarget);
                var btn = event.currentTarget;
            }
            if (btn.classList.contains("empty_seat")) {
                btn.classList.remove("empty_seat");
                btn.classList.add("reserved_seat");
                summaryPrice += standartPrice;
            } else if (btn.classList.contains("empty_left_lux_seat")) {
                btn.classList.remove("empty_left_lux_seat");
                btn.classList.add("reserved_left_lux_seat");
                summaryPrice += comfortPrice;
            } else if (btn.classList.contains("empty_right_lux_seat")) {
                btn.classList.remove("empty_right_lux_seat");
                btn.classList.add("reserved_right_lux_seat");
                summaryPrice += comfortPrice;
            }
            updateSumaryPrice();

            var currentRow = btn.getAttribute("row");
            var currentSeat = btn.getAttribute("seat");
            var currentPrice = btn.getAttribute("price");
            var btnId = btn.getAttribute("id");
            sideMenu.innerHTML = sideMenu.innerHTML + '<div id="ticket' + btn.getAttribute("id") + '" class"ticket"><h4>' +
                "Ряд " + currentRow + ", Место " + currentSeat +
                ", цена " + currentPrice + " ГРН" +
                '</h4></div>';

            ticketInBucketCounter++;
            updateSumaryPrice();
            btn.onclick = function (event) {
                unreserveSeat(reserve);
            };
            btn.setAttribute("onclick", "unreserveSeat()");

            var price = currentPrice;
            var rowNumber = currentRow;
            var seatNumber = currentSeat;
            var values = {price, movieSessionId, rowNumber, seatNumber, hallId};
            order.set(btnId, values);
            localStorage.setItem(movieSessionId + btnId, JSON.stringify(values));
            //////////////////////////////////////////////////////////////////////
        } else alert("нельзя за раз больше 9 билетов");

    }

    function updateSumaryPrice() {
        var sum = document.getElementById("summaryPrice");
        sum.innerHTML = "Всего: " + summaryPrice + " грн";
    }
    //снять резерв места по клику
    function unreserveSeat() {
        var btn = event.currentTarget;
        var btnId = btn.getAttribute("id");
        if (btn.classList.contains("reserved_seat")) {
            summaryPrice -= standartPrice;
        } else {
            summaryPrice -= comfortPrice;
        }
        btn.classList.remove("reserved_seat");
        btn.classList.remove("reserved_left_lux_seat");
        btn.classList.remove("reserved_right_lux_seat");
        btn.classList.add(btn.getAttribute("reserve"));
        var ticket = document.getElementById("ticket" + btn.id);
        ticket.remove();
        btn.onclick = function () {
            reserveSeat();
        };
        order.delete(btn.getAttribute("id"));
        ticketInBucketCounter--;
        updateSumaryPrice();
        localStorage.removeItem(movieSessionId + btnId);
    }


    //отобразить заполненные места
    <c:forEach var="ticket" items="${tickets}">
    var rowNumber = <c:out value = "${ticket.rowNumber}"/>;
    var seatNumber = <c:out value = "${ticket.seatNumber}"/>;
    console.log(rowNumber);
    console.log(seatNumber);
    var btn = document.getElementById("but" + rowNumber + seatNumber);
    btn.classList.remove("empty_seat");
    btn.classList.remove("empty_left_lux_seat");
    btn.classList.remove("empty_right_lux_seat");
    btn.classList.add("used_seat");
    btn.onclick = "";
    </c:forEach>
</script>

</body>
</html>
