<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
    <title><spring:message code="film.page.list.message" /></title>
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cerulean/bootstrap.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>

<h1><spring:message code="film.page.list.message" />:</h1>
<button id="btnAdd">New</button>
<table class="table table-striped" id="tblData">
    <tr>
        <th style="display:none;">filmId</th>
        <th>name</th>
        <th>description</th>
        <th>duration</th>

    </tr>
    <c:forEach var="film" items="${allFilms}">
    <tr>
        <td style="display:none;">${film.filmId}</td>
        <td>${film.name}</td>
        <td>${film.description}</td>
        <td>${film.duration}</td>
        <td><a href='#' class='btnDelete'>DELETE</a>&nbsp;<a href='#' class='btnEdit'>EDIT</a></td>
    </tr>
    </c:forEach>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/functions.js"></script>
</body>
</html>
