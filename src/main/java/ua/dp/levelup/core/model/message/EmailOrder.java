package ua.dp.levelup.core.model.message;

import lombok.Getter;
import lombok.Setter;
import ua.dp.levelup.core.model.Order;

@Getter
@Setter
public class EmailOrder extends MessageForUsers {

    private Order order;

    public EmailOrder(String recipient, String subject, Order order) {
        super(recipient, subject);
        this.order = order;
    }
}
