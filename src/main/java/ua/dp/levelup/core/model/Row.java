package ua.dp.levelup.core.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@ToString(exclude = "seats")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ROWS")
public class Row {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long rowId;

    @Column(nullable = false)
    private int rowNumber;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "row")
    private List<Seat> seats;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hall_id", nullable = false)
    private Hall hall;

    public Row(int rowNumber, Hall hall) {
        this.rowNumber = rowNumber;
        this.hall = hall;
    }

    public Row(int rowNumber, List<Seat> seats) {
        this.rowNumber = rowNumber;
        this.seats = seats;
    }

}
