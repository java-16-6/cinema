package ua.dp.levelup.core.model.message;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public abstract class MessageForUsers {

    public String recipient;
    public String subject;

    public MessageForUsers(String recipient, String subject) {
        this.recipient = recipient;
        this.subject = subject;
    }
}
