package ua.dp.levelup.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProtectedController {

    @RequestMapping("/protected")
    public String getProtectedPage() {
        return "Hello";
    }
}
