package ua.dp.levelup.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.Rate;
import ua.dp.levelup.core.model.filters.Filters;
import ua.dp.levelup.service.CommentService;
import ua.dp.levelup.service.FilmService;
import ua.dp.levelup.service.MovieSessionService;
import ua.dp.levelup.service.RateService;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/film")
public class FilmController {

    public static final Logger LOGGER = Logger.getLogger(FilmController.class);

    @Autowired
    private FilmService filmService;

    @Autowired
    private RateService rateService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private MovieSessionService movieSessionService;

    @Autowired
    private Filters filters;

    @Value("${comments.on.page}")
    private int countCommentsOnPage;

    @RequestMapping("/list")
    public ModelAndView getAllFilms() {
        List<Film> allFilms = filmService.getAllFilms();
        ModelAndView modelAndView = new ModelAndView("film-list");
        modelAndView.addObject("allFilms", allFilms);
        modelAndView.addObject("pageTitle", "film-list.page.title");

        return modelAndView;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView getFilmById(@PathVariable("id") long id) {
        Film film = filmService.getFilmById(id);
        Collections.sort(film.getSessionList(), filters.sessionComparator());
        Set<Date> uniqueDates = movieSessionService.getUniqueDatesBySessions(film.getSessionList());
        int countComments = commentService.getCountCommentsByFilm(film.getFilmId());

        double averageRating = rateService.getAverageRatingForFilm(id);
        LOGGER.info(averageRating);

        //todo Replace with real currentUserId
        //also, user with test id must exist in database
        Long currentUserId = 5L;

        ModelAndView modelAndView = new ModelAndView("film-page");

        //just skip exception if user hasn't voted before
        try {
            Rate currenUsersRateForThisFilm = rateService.getRateByUserIdAndFilmId(currentUserId, id);
            modelAndView.addObject("userRate", currenUsersRateForThisFilm);
        } catch (NoResultException e) {
            LOGGER.error(e);
            modelAndView.addObject("userRate", null);
        }

        modelAndView.addObject("uniqueDates", uniqueDates);
        modelAndView.addObject("countComments", countComments);
        modelAndView.addObject("countCommentsOnPage", countCommentsOnPage);
        modelAndView.addObject("film", film);
        modelAndView.addObject("averageRating", averageRating);
        modelAndView.addObject("userId", currentUserId);

        return modelAndView;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String createFilm(@RequestBody Film film) {
        filmService.createFilm(film);
        return "redirect:/film/list";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String removeAd(@RequestParam("filmId") long filmId) {
        filmService.deleteFilmById(filmId);
        return "redirect:/film/list";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String updateFilm(@RequestBody Film film) {
        filmService.updateFilm(film);
        return "redirect:/film/list";
    }

    @RequestMapping(value = "/add-film", method = RequestMethod.GET)
    public ModelAndView addFilm() {
        ModelAndView modelAndView = new ModelAndView("add-film-page");
        return modelAndView;
    }

    @ResponseBody
    @RequestMapping(value = "/get-film-image/{id}", method = RequestMethod.GET, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE})
    public byte[] getImageFilm(@PathVariable("id") long id) throws IOException {
        Film film = filmService.getFilmById(id);
        return filmService.getFilmImage(film);
    }

    @RequestMapping(value = "/process-adding-image/{id}", method = RequestMethod.POST)
    public String processAddingFilm(@PathVariable("id") long id,
                                    @RequestParam("file") MultipartFile file,
                                    @RequestHeader("referer") String referer,
                                    RedirectAttributes redirectAttributes) throws IOException {
        if (!file.isEmpty() && (file.getContentType().equals(MediaType.IMAGE_JPEG_VALUE) ||
                file.getContentType().equals(MediaType.IMAGE_PNG_VALUE))) {
            try {
                Film film = filmService.getFilmById(id);
                filmService.uploadImage(file, film);
            } catch (Exception e) {
                redirectAttributes.addFlashAttribute("message", "Unable to read image" + e);
                return "redirect:" + referer;
            }
        } else {
            redirectAttributes.addFlashAttribute("message", "Unable to read image");
            return "redirect:" + referer;
        }
        redirectAttributes.addFlashAttribute("message", "Your image successfully uploaded ");
        return "redirect:" + referer;
    }
}