package ua.dp.levelup.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@Configuration
@EnableJpaRepositories(basePackages = {"ua.dp.levelup.repository"}, entityManagerFactoryRef = "emf")
//@EnableJpaAuditing(dateTimeProviderRef = "dateTimeProvider")
@EnableSpringDataWebSupport
public class JPARepositoryConfig {

}
