package ua.dp.levelup.web.config.tiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.accept.PathExtensionContentNegotiationStrategy;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class TilesConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(TilesConfig.class);

    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions("/WEB-INF/tiles/config/templates.xml");
        return tilesConfigurer;
    }

    @Bean
    public ViewResolver viewResolver() {
        UrlBasedViewResolver tilesViewResolver = new UrlBasedViewResolver();
        tilesViewResolver.setViewClass(TilesView.class);
        return tilesViewResolver;
    }

    @Bean
    public ContentNegotiatingViewResolver contentNegotiatingViewResolver() {
        ContentNegotiatingViewResolver contentNegotiatingViewResolver = new ContentNegotiatingViewResolver();
        contentNegotiatingViewResolver.setContentNegotiationManager(contentNegotiationManager());
        return contentNegotiatingViewResolver;
    }

    public ContentNegotiationManager contentNegotiationManager() {
        return new ContentNegotiationManager(pathExtensionContentNegotiationStrategy());
    }

    public PathExtensionContentNegotiationStrategy pathExtensionContentNegotiationStrategy() {
        Map<String, MediaType> map = new HashMap<>();
        map.put("html", MediaType.valueOf("text/html"));
        map.put("pdf", MediaType.valueOf("application/pdf"));
        map.put("xsl", MediaType.valueOf("application/vnd.ms-excel"));
        map.put("xml", MediaType.valueOf("application/xml"));
        map.put("json", MediaType.valueOf("application/json"));
        map.put("atom", MediaType.valueOf("application/xml"));
        return new PathExtensionContentNegotiationStrategy(map);
    }

}
