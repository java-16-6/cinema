package ua.dp.levelup.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.app.Comment;

import java.util.List;

@org.springframework.stereotype.Repository
@Transactional
public interface CommentRepository extends Repository<Comment, Long> {

    List<Comment> findByFilm(Film film, Pageable pageable);

    @Query(value = "SELECT count(c.film_id) FROM COMMENTS c WHERE " +
            "c.film_id=:filmId",
            nativeQuery = true
    )
    int getCountCommentsByFilm(@Param("filmId") Long filmId);

    int countCommentsByFilm(Film film);
}