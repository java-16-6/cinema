package ua.dp.levelup.service;

import org.springframework.data.domain.Pageable;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.app.Comment;

import java.util.List;

public interface CommentService {
    void createComment(Comment comment);

    List<Comment> getAllComments();

    Comment getCommentById(long commentId);

    void updateComment(Comment comment);

    void deleteComment(Comment comment);

    List<Comment> findByFilm(Film film, Pageable pageable);

    Pageable createPageRequest(int numberPage);

    int countCommentsByFilm(Film film);

    int getCountCommentsByFilm(Long filmId);
}