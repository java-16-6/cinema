package ua.dp.levelup.service;

import ua.dp.levelup.core.model.Rate;

import java.util.List;

public interface RateService {
    void createRate(Rate rate);

    List<Rate> getAllRates();

    Rate getRateById(Long id);

    void updateRate(Rate rate);

    void deleteRate(Rate rate);

    double getAverageRatingForFilm(Long filmId);

    Rate getRateByUserIdAndFilmId(Long userId, Long filmId);
}
