package ua.dp.levelup.service;

import ua.dp.levelup.core.model.MovieSession;
import ua.dp.levelup.core.model.dto.MovieSessionDto;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface MovieSessionService {

    void createMovieSession(MovieSession session);

    MovieSession getMovieSessionById(long sessionId);

    List<MovieSession> getAllMovieSessions();

    List<MovieSession> getAllMovieSessionsForToday();

    List<MovieSessionDto> getAllMovieSessionByDate(Date date);

    List<MovieSession> getMovieSessionsByDate(Date date);

    Set<Date> getUniqueDatesBySessions(List<MovieSession> movieSessions);
}
