package ua.dp.levelup.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.MovieSession;
import ua.dp.levelup.core.model.dto.MovieSessionDto;
import ua.dp.levelup.dao.MovieSessionDao;
import ua.dp.levelup.service.FilmService;
import ua.dp.levelup.service.MovieSessionService;
import ua.dp.levelup.utils.CinemaUtilityFunctions;

import java.util.*;

@Service("movieSessionService")
public class MovieSessionServiceImpl implements MovieSessionService {

    private static final Logger LOGGER = Logger.getLogger(MovieSessionServiceImpl.class);

    private MovieSessionDao movieSessionDao;

    @Autowired
    private CinemaUtilityFunctions cinemaUtilityFunctions;

    @Autowired
    private FilmService filmService;

    private List<MovieSession> todayMovieSessions = new ArrayList<>();

    @Autowired
    public void setMovieSessionDao(final MovieSessionDao movieSessionDao) {
        this.movieSessionDao = movieSessionDao;
    }

    @Override
    public void createMovieSession(MovieSession session) {
        movieSessionDao.createMovieSession(session);
        if (cinemaUtilityFunctions.isSameDay(new Date(), session.getSessionStartDate())) {
            todayMovieSessions.add(session);
        }
    }

    @Override
    public MovieSession getMovieSessionById(long sessionId) {
        return movieSessionDao.getMovieSessionById(sessionId);
    }

    @Override
    public List<MovieSession> getAllMovieSessions() {
        return movieSessionDao.getAllMovieSessions();
    }

    @Override
    public List<MovieSession> getAllMovieSessionsForToday() {
        if (todayMovieSessions.isEmpty()) {
            updateMovieSessionsForToday();
        }
        return todayMovieSessions;
    }

    @Override
    public List<MovieSessionDto> getAllMovieSessionByDate(Date date) {
        List<MovieSession> allMovieSessionByDate = movieSessionDao.getAllMovieSessionByDate(date);

        Map<Long, List<MovieSession>> listMap = assembleMovieSessionsByFilmId(allMovieSessionByDate);

        List<MovieSessionDto> allSessionsByFilm = collectMovieSessionsDto(listMap);
        return sortMovieSessionsByStartTime(allSessionsByFilm);
    }

    private List<MovieSessionDto> collectMovieSessionsDto(Map<Long, List<MovieSession>> listMap) {
        List<MovieSessionDto> allSessionsByFilm = new ArrayList<>();
        for (Map.Entry<Long, List<MovieSession>> entry : listMap.entrySet()) {
            Long filmId = entry.getKey();
            Film film = filmService.getFilmById(filmId);

            allSessionsByFilm.add(new MovieSessionDto(film, entry.getValue()));
        }
        return allSessionsByFilm;
    }

    private Map<Long, List<MovieSession>> assembleMovieSessionsByFilmId(List<MovieSession> allMovieSessionByDate) {
        Map<Long, List<MovieSession>> listMap = new HashMap<>();

        for (MovieSession session : allMovieSessionByDate) {
            if (listMap.containsKey(session.getFilm().getFilmId())) {
                List<MovieSession> sessionList = listMap.get(session.getFilm().getFilmId());
                sessionList.add(session);
            } else {
                List<MovieSession> sessionList = new ArrayList<>();
                sessionList.add(session);
                listMap.put(session.getFilm().getFilmId(), sessionList);
            }
        }
        return listMap;
    }

    private List<MovieSessionDto> sortMovieSessionsByStartTime(List<MovieSessionDto> allSessionsByFilm) {
        for (MovieSessionDto dto : allSessionsByFilm) {
            Collections.sort(dto.getSessionList(), (e1, e2) ->
                    Long.compare(e1.getSessionStartTime().getTime(), e2.getSessionStartTime().getTime()));
        }
        return allSessionsByFilm;
    }

    public Set<Date> getUniqueDatesBySessions(List<MovieSession> movieSessions) {
        Set<MovieSession> setSessions = new LinkedHashSet<>(movieSessions);
        Set<Date> uniqueDates = new TreeSet<>();
        for (MovieSession ms : setSessions) {
            uniqueDates.add(ms.getSessionStartDate());
        }
        return uniqueDates;
    }

    @Override
    public List<MovieSession> getMovieSessionsByDate(Date date) {
        return movieSessionDao.getMovieSessionsByDate(date);
    }

    //Every day at 00:01
    @Scheduled(cron = "0 1 0 * * *")
    private void updateMovieSessionsForToday() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(">>> updateMovieSessionsForToday triggered " + (todayMovieSessions.isEmpty() ? " first time!" : " by timer!"));
        }
        todayMovieSessions = movieSessionDao.getMovieSessionsForToday();
    }


}
