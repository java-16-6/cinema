package ua.dp.levelup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.levelup.core.model.Row;
import ua.dp.levelup.core.model.Seat;
import ua.dp.levelup.dao.SeatDao;
import ua.dp.levelup.service.SeatService;

import java.util.List;

@Service
public class SeatServiceImpl implements SeatService {

    private SeatDao seatDao;

    @Autowired
    public void setSeatDao(SeatDao seatDao) {
        this.seatDao = seatDao;
    }

    @Override
    public void createSeat(Seat seat) {
        seatDao.createSeat(seat);
    }

    @Override
    public List<Seat> getAllSeats() {
        return seatDao.getAllSeats();
    }

    @Override
    public Seat getSeatById(Long id) {
        return seatDao.getSeatById(id);
    }

    @Override
    public void updateSeat(Seat seat) {
        seatDao.updateSeat(seat);
    }

    @Override
    public void deleteSeat(Seat seat) {
        seatDao.deleteSeat(seat);
    }

    @Override
    public void createAllSeats(List<Row> rows) {
        for (Row row : rows) {
            for (Seat seat : row.getSeats()) {
                seat.setRow(row);
                createSeat(seat);
            }
        }
    }
}