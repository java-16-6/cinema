package ua.dp.levelup.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.app.Comment;
import ua.dp.levelup.dao.CommentDao;
import ua.dp.levelup.repository.CommentRepository;
import ua.dp.levelup.service.CommentService;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private CommentDao commentDao;
    private CommentRepository commentRepository;

    @Value("${comments.on.page}")
    private int commentsOnPage;

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Autowired
    public void setCommentDao(final CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    @Override
    public void createComment(Comment comment) {
        commentDao.createComment(comment);
    }

    @Override
    public List<Comment> getAllComments() {
        return commentDao.getAllComments();
    }

    @Override
    public Comment getCommentById(long commentId) {
        return commentDao.getCommentById(commentId);
    }

    @Override
    public void updateComment(Comment comment) {
        commentDao.updateComment(comment);
    }

    @Override
    public void deleteComment(Comment comment) {
        commentDao.deleteComment(comment);
    }

    @Override
    public List<Comment> findByFilm(Film film, Pageable pageable) {
        return commentRepository.findByFilm(film, pageable);
    }

    @Override
    public Pageable createPageRequest(int numberPage) {
        return new PageRequest(numberPage, commentsOnPage, new Sort(Sort.Direction.DESC, "dateComment"));
    }

    @Override
    public int countCommentsByFilm(Film film) {
        return commentRepository.countCommentsByFilm(film);
    }

    @Override
    public int getCountCommentsByFilm(Long filmId) {
        return commentRepository.getCountCommentsByFilm(filmId);
    }
}
