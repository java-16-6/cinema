package ua.dp.levelup.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.levelup.core.model.Rate;
import ua.dp.levelup.dao.RateDao;
import ua.dp.levelup.service.RateService;

import java.util.List;

@Service("rateService")
public class RateServiceImpl implements RateService {

    private RateDao rateDao;
    public static final Logger LOGGER = Logger.getLogger(RateServiceImpl.class);

    @Autowired
    public void setRateDao(final RateDao rateDao) {
        this.rateDao = rateDao;
    }

    @Override
    public void createRate(Rate rate) {
        rateDao.createRate(rate);
    }

    @Override
    public List<Rate> getAllRates() {
        return rateDao.getAllRates();
    }

    @Override
    public Rate getRateById(Long id) {
        return rateDao.getRateById(id);
    }

    @Override
    public void updateRate(Rate rate) {
        LOGGER.info(rate);

        if (rate.getRateId() == null) {
            rateDao.createRate(rate);
        } else {
            rateDao.updateRate(rate);
        }
    }

    @Override
    public void deleteRate(Rate rate) {
        rateDao.deleteRate(rate);
    }

    @Override
    public double getAverageRatingForFilm(Long filmId) {
        return rateDao.getAverageRatingForFilm(filmId);
    }

    @Override
    public Rate getRateByUserIdAndFilmId(Long userId, Long filmId) {
        return rateDao.getRatesByUserAndFilmId(userId, filmId);
    }
}
