package ua.dp.levelup.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.app.Comment;
import ua.dp.levelup.dao.CommentDao;

import java.util.List;

@Repository
@Transactional
public class CommentDaoImpl implements CommentDao {

    @Autowired
    private HibernateTemplate hibernateTemplate;

    @Autowired
    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    public void createComment(Comment comment) {
        hibernateTemplate.save(comment);
    }

    @Override
    public List<Comment> getAllComments() {
        return hibernateTemplate.loadAll(Comment.class);
    }

    @Override
    public Comment getCommentById(long commentId) {
        return hibernateTemplate.load(Comment.class, commentId);
    }

    @Override
    public void updateComment(Comment comment) {
        hibernateTemplate.update(comment);
    }

    @Override
    public void deleteComment(Comment comment) {
        hibernateTemplate.delete(comment);
    }
}
