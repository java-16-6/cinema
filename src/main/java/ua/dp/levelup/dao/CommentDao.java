package ua.dp.levelup.dao;

import ua.dp.levelup.core.model.app.Comment;

import java.util.List;

public interface CommentDao {

    void createComment(Comment comment);

    List<Comment> getAllComments();

    Comment getCommentById(long commentId);

    void updateComment(Comment comment);

    void deleteComment(Comment comment);
}
